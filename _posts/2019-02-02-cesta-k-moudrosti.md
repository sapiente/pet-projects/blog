---
layout: post
title: Cesta k moudrosti 
description: Blog o softwarovém inženýrství
 
permalink: /blog/cesta-k-moudrosti
cover: /assets/images/2019-02-02/cover.jpg
image: /assets/images/2019-02-02/cover.jpg
---

> Moudrost je aplikace znalosti, soudnosti, vhledu, zkušenosti a úsudku, aby mohl člověk udělat dobré rozhodnutí tam, kde odpověď nemusí být zřejmá.
>
>-- Velcí šéfové - Jak vést celý život (Kenneth Blanchard)

Již delší dobu se snažím definovat své já a určit cestu, kterou bych chtěl kráčet. Bohužel stále nemohu jasně říci, co by na konci té cesty mělo být, co bych chtěl v životě dokázat a jednou zanechat. Co ale mohu, je držet se své fundamentální hodnoty - **růst**. 

- Věřím, že **poznání** je prvním krokem k **uvědomění**, které vede ke **zlepšení**. 
- Věřím, že **vědomosti** jsou nástrojem pro nalezení lepších cest, které nás vedou k **moudrosti**. 
- A rozhodně věřím, že kdo přestane růst, je technicky i spirituálně stejně tak dobrý, jako mrtvý.

Rád bych proto s kůží na trh a prostřednictvím tohoto blogu sdílel svou cestu životem za moudrostí. Aktuálně pracuji jako Java vývojář v [Brněnské IT firmě](https://www.morosystems.cz/){:target="_blank" rel="noopener noreferrer nofollow"} se specializací na webové technologie a zájmem v leadershipu, managementu, řízení podniků a seberozvoji.

Nejlepším způsobem, jak se učit, je učit. Proto bych zde rád psal o různých tématech, abych mohl dále růst, a třeba i inspirovat druhé. Primárně se budu zaměřovat na [softwarové inženýrství](https://en.wikipedia.org/wiki/Software_engineering){:target="_blank" rel="noopener noreferrer nofollow"}, jakožto technickou disciplínu zabývající se všemi aspekty produkce softwaru, která se narozdíl od informatiky zabývá praktickými otázkami vývoje a poskytování užitečného softwaru.

Velký impuls pro založení vlastního blogu přišel po objevení úžasného blogu [sw-samuraj](https://sw-samuraj.cz/){:target="_blank" rel="noopener noreferrer nofollow"}, který vřele doporučuji pročíst. Přestože je propast znalostí a zkušeností značně hluboká, každý si svůj příběh píšeme sami. A můj příběh najdete zde.