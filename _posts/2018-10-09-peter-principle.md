---
title: Peter principle
description: Jaká je příčina nekompetence některých vedoucích pracovníků? Jak se jí dá vyvarovat?

permalink: /blog/peter-principle
cover: /assets/images/2018-10-09/cover.jpg
image: /assets/images/2018-10-09/cover.jpg

layout: post
---

> Peter principle - povyšování do nejvyšší míry nekompetence

Doktor **Laurence J. Peter**, který sám sebe označoval za *hierarchologistu*, se zabýval organizačními hierarchiemi a chováním jejich členů. Všiml si jedné zajímavé věci - lidé v hierarchii mají tendenci být povyšováni až do pozice, kterou nedělají dobře. Jak je něco takového ve firmách vůbec možné?

## Příčina

Tento jev je možné pozorovat u vedení firem, které nepracují na rozvoji svých zaměstnanců. Je to docela typické u malých podniků, kde se hierarchie organizace buduje postupně. Prakticky to funguje následovně - vedení si všimne nadprůměrného výkonu zaměstnance a rozhodne se ho povýšit. Má vizi, že povýšený zaměstnanec bude schopen pozvednout výkon ostatních. Samotná myšlenka není špatná, problém nastává tehdy, když vedení nevezme v potaz povahu a hodnoty daného zaměstnance. Například pokud je to individualista, jak můžeme očekávat, že najednou zvládne hrát týmově? Pokud je to introvert, jak můžeme očekávat, že najednou zvládne inspirovat a motivovat ostatní? Není špatné takové lidi povýšit. Jen je potřeba s nimi pracovat na daných hodnotách a schopnostech, které jsou na dané pozici potřeba.

## Důsledek

Případ, který jsem sám zažil, byl až učebnicový. V jedné vývojové firmě nastoupil nový seniorní vývojář, který měl zastávat roli projektového manažera a leadera. Je docela běžné, že se ze zkušených vývojářů stávají projektoví leadeři a někdy manažeři. Problém spočívá v tom, že se jim nepředávají dané hodnoty a nerozvíjí schopnosti potřebné pro vedení lidí či řízení projektů a zdrojů. Postupem času se jeho nekompetence začala projevovat. Lidé nevěděli na čem mají pracovat nebo kdo na čem už pracuje. Jaká práce je hotová, jaká zbývá. Tým nevěděl kam směřuje, jaké jsou aktuální priority, nebo jak velký je technický dluh. Také byla často podkopávána morálka týmu, jelikož si manažer neustále na něco stěžoval. Nebylo žádné vedení příkladem, žádná inspirace a povzbuzování.  Vedení firmy zajímalo jedno jediné **KPI (key performance indicator)** a to ohledně množství vykázané práce. Žádná KPI ohledně množství odvedené práce, její kvality či samotného využití zdrojů. V podstatě nikdo neřešil, kam projekt směřuje, a za co klient vlastně platí. Důležité bylo, aby platil. Situace se však místo k lepšímu změnila k horšímu. Manažer byl zvolen novým technickým ředitelem s úkolem zlepšit kvalitu vývoje. Jak můžete očekávat zlepšení kvality od někoho, kdo nejen, že nechápe míru její důležitosti, ale neví, jak kvalitu posoudit a kultivovat?  

Abyste mě nepochopili špatně, samotný povýšený manažer nebyl jádrem problému, byl pouze projevem. Jádrem problému byla absence individuálního přístupu k němu, vedení s ním nepracovalo na jeho rozvoji. Jak říká Simon Sinek - každý má kapacitu být leaderem, ne každý by jím ale být měl. Každý se může dostat na určitou úroveň, naučit se dané dovednosti, získat dané hodnoty a znalosti. Jen se s ním na tom musí pracovat. Jak k tomu tedy přistupovat a ideálně preventivně předcházet?

## Řešení a prevence

Jeden skvělý způsob je ve formě **kompetenčních modelů**. V podstatě se sepíše soubor hodnot a schopností, kterými by měl člověk na dané pozici disponovat. Důležité je, že se tyto prvky formulují, a dá se o nich komunikovat. Zároveň slouží kompetenční model jako jakási forma cílů. Pokud je stanovený kompetenční model na pozici projektového leadera a v týmu je člen, kterého by tato role naplňovala, má možnost se postupně připravovat. Toto bývá často předmětem individuálních meetingů (1:1) mezi zaměstnancem a jeho nadřízeným, kde se právě rozebírá, jak zlepšení kompetencí na aktuální pozici, tak případné nabytí kompetencí na budoucí pozici. Nepleťme si prosím kompetenční model s **kulturou**. V kultuře se řeší hodnoty firmy jako celku - co je pro firmu důležité, čeho si váží a jaká chce být. V kompetenčním modelu se řeší hodnoty jednotlivce, které přispívají k celku. Například jiný kompetenční model bude na pozici finančního ředitele a jiný na pozici sekretářky. Obě pozice přispívají k celkovému výsledku, každá ale trošku jinak a s jiným přístupem.
  
Dalším způsobem je **retrospektiva** a **zpětná vazba**. Tyto dvě metody představují klíčové prkvy v **demingově cyklu (PDCA)** a technice **kaizen**. Jedná se o validaci postupu a jeho důsledcích. Retrospektivu můžeme aplikovat sami jako sebereflexi nebo týmově skrze některé techniky. Zde je potřeba dávat pozor na neobjektivitu. K tomu je důležitá zpětná vazba jiné osoby. Ideální je opět skrze individuální meetingy (1:1) s nadřízeným nebo sám se svými podřízenými. Dostávat zpětnou vazbu od podřízených by upřímně mělo více zaměstnanců na vedoucích pozicích. Dostávat zpětnou vazbu od nadřízeného není o tom dostat jasné odpovědi co je špatně a co zlepšit. Běžně je nadřízeným někdo, kdo se v dané doméně vyzná méně, či má dokonce méně zkušeností. Vedoucími pracovníky bývají lidé, kteří umí pracovat s jinými lidmi. Dokáží lépe najít příčinu problémů, dát vhled skrze dobře formulovanou otázku - zkrátka často slouží jen jako hlas rozumu. Jejich konečným cílem je pak tyto schopnosti předat svým podřízeným - leader buduje leadera. Podřízení jim však jako jediní mohou upřímně říct, jaký dopad na ně chování nadřízeného má, proto je dobré kombinovat oba dva přístupy.


## Zdroje
* [https://www.toolshero.com/management/the-peter-principle/](https://www.thebalancecareers.com/the-peter-principle-2275684){:target="_blank" rel="noopener noreferrer nofollow"}
* [https://www.forbes.com/sites/roddwagner/2018/04/10/new-evidence-the-peter-principle-is-real-and-what-to-do-about-it/#7aefa5641809](https://www.forbes.com/sites/roddwagner/2018/04/10/new-evidence-the-peter-principle-is-real-and-what-to-do-about-it/#7aefa5641809){:target="_blank" rel="noopener noreferrer nofollow"}
* [https://www.investopedia.com/terms/p/peter-principle.asp](https://www.investopedia.com/terms/p/peter-principle.asp){:target="_blank" rel="noopener noreferrer nofollow"}
* [https://www.amazon.com/Peter-Principle-Things-Always-Wrong/dp/0062092065](https://www.amazon.com/Peter-Principle-Things-Always-Wrong/dp/0062092065){:target="_blank" rel="noopener noreferrer nofollow"}
* [Simon Sinek - Get people to follow you](https://youtu.be/lPwYUDD9Pd4){:target="_blank" rel="noopener noreferrer nofollow"}


