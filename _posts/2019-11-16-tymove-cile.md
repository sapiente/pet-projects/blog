---
title: Jak jsme zavedli týmové cíle
description: Cesta za zvýšením angažovanosti a zapojení lidí v týmu

permalink: /blog/jak-jsme-zavedli-tymove-cile
cover: /assets/images/2019-11-16/cover.jpg
image: /assets/images/2019-11-16/cover.jpg

layout: post
---
Strategické řízení cíli je bezesporu důležitá část řízení organizace. Je ale také částí velice náročnou, neboť je těžké oslovit a motivovat lidi na všech úrovních organizační struktury. Naše firma pracuje s cíli na samém vrcholu a níže na úrovni našich business jednotek, odděleních chcete-li. To bohužel pro lidi, kteří tvoří samotnou produkci, nestačí. Všiml jsem si toho u našich vývojářů. Pár lidí se na jednom vyhodnocení kvartálu naší business jednotky, kde se ukazovali finanční výsledky a cíle na následující kvartál, ptalo, co to pro ně vlastně znamená. Co s tím vlastně mohou oni udělat, aby ve výsledcích byla barva zelená místo oranžové či červené. Jak tomu mohou pomoci, než jen chodit do práce, jak mají. Než jen pracovat tak kvalitně, jak dokážou. Ta otázka mě oslovila a začal jsem přemýšlet, jak bychom to mohli změnit. S požehnáním personal lídra a delivery manažerky na projektu jsem pak s týmem zavedl týmové cíle.

## Plán

Chtěli jsme tedy docílit zvýšení angažovanosti a zapojení týmu skrze definování jasné představy, kam jako tým směřujeme a jak se posouváme. Tím dostane každý v týmu příležitost přispět v konečném důsledku business jednotce a firmě. 

Cíle musí být pro lidi v týmu jasně představitelné a musí být schopni se s nimi ztotožnit. Musí tedy jít o jistou kombinaci bottom-up a top-down metody rozpadu cílů. Záměrem je totiž namapovat strategické cíle na lidi, kteří každý den produkují hodnotu pro firmu. Cíle musí být relevantní pro projekt, na kterém tým pracuje. Nemá cenu si dát za cíl rozvoj umělé inteligence, pokud to projekt nevyužije. Na to jsou jiné cesty jako osobní rozvojové plány, či speciální firemní inovativní skupina. Tým si tedy může cíle vybrat sám, jen je potřeba je udržet v prostoru ohraničeném cíli stakeholderů. Případně si odůvodnit, proč je mimo prostor a jak to projektu pomůže. Proto tedy kombinace bottom-up a top-down způsobu.

Můj plán byl docela prostý, vycházel z metodiky OKR (Objective Key Results). Když bude zájem, mohu metodiku popsat v jiném článku. Zde je kdyžtak ukázka, jak takové cíle pomocí ORK vypadají: [https://okrexamples.co/](https://okrexamples.co/){:target="_blank" rel="noopener noreferrer nofollow"}.

**Plán vypadal zhruba následovně:**
* zjistí se a sepíší cíle všech stakeholderů: account plán delivery manažera, rozvojový plán personal lídra, rozvojový plán tým lídra a kvartální plán business jednotky,
* proběhne plánovací meeting s celým týmem, případně těmi, kteří chtějí participovat, kde se sepíší cíle, měřitelné výsledky a ty se pak rozpadnou na akční kroky,
* každý týden, později každý druhý, proběhne tzv. check-in meeting, kde se zapíše progres a domluví další akční kroky pro naplnění cíle.

## Realizace

Jak jsme tedy týmové cíle zavedli? Řekl bych, že docela jednoduše a zkusím to popsat chronologicky.

### 1. Příprava

> Many people think they lack motivation when what they really lack is clarity  
> -- James Clear, Atomic Habits

Nejdříve jsem si sepsal plán, jak by to mohlo vypadat a probíhat. To bylo důležité, abychom všichni věděli, kolik času a energie nás to bude stát a co to znamená na denodenní operativě. 

Pak bylo důležité dostat požehnání delivery manažera kvůli nefakturovatelnému času, který se přirozeně začne promítat do zisku projektu. Díky tomu, že revenue neplánujeme alibisticky na 100 % kapacit lidí, tak jsme si jen zkontrolovali, že se v měsíci vlezeme do našeho bufferu pro nefakturovatelný čas. Případně přirozená otázka - kolik jsme ochotni investovat a jak si vyhodnotíme přínos.

Po vymyšlení plánu a požehnání vedení, jsem domluvil meeting, kde jsem celý koncept představil a získal zpětnou vazbu. Nebyl to kick-off meeting, kde se zahajuje projekt či aktivita. Jedním ze záměrů týmových cílů je zvýšení angažovanosti. Nelze jednoduše přijít a autoritativně říct “tohle teď budeme takhle dělat”. Je potřeba lidi motivovat a nadchnout pro věc. Klidně to pojmout formou experimentu, který lidi přijmou i při mírném nesouhlasu, protože ví, že to je dočasné. Na meetingu jsem tedy představil celý koncept, čeho tím mohou dosáhnout a jak jim to může pomoct. Reakce byla vesměs pozitivní, ale některé problémy a nejasnosti se vyslovili. Největší obava byla, že by se tomu musel někdo věnovat ve volném čase. Na to byla odpověď relativně jednoduchá. Týmové cíle mohou být dvou typů. První nám pomůže hned, posune něco, co už děláme, ať už špatně či dobře. Například agilní řízení vývoj, který nějak děláme, ale můžeme ho nějak zlepšit. Takové cíle zakomponujeme do projektu jako úkoly, které zaplatí ideálně klient. Druhý nám může pomoci v budoucnu, poskytne nám tedy nějakou příležitost a možnosti. Například zvýšení zastupitelnosti či rozvoj kompetencí, které se projeví v dlouhodobém horizontu. Takové cíle zakomponujeme do rozpočtu firemní inovativní skupiny či do rozpočtu na individuální rozvoj lidí. Pokud jsou lidé ochotni se věnovat cílům i ve volném čase, je to skvělý benefit a jejich vizitka. Nedá se ale vynutit zapojení lidí, kteří mají jiné priority v nepracovním čase, nebo nejsou úplně přesvědčeni přínosem. Je třeba poskytnout patřičné podmínky pro práci s cíli a postupně pracovat na motivaci lidí. Když je to bude bavit a uvidí přínos, může se jejich přístup změnit.

### 2. Plánování

Když jsme si s týmem tedy potvrdili, že chceme koncept vyzkoušet, tak jsme si domluvili meeting pro naplánování cílů. Každý měl možnost si dopředu přečíst cíle stakeholderů a vymyslet tak pár kandidátů na týmové cíle. Na samotném meetingu jsme nejdříve prošli cíle stakeholderů a vytáhli z nich ty, které jsou relevantní pro náš tým. Jako tým nemůžete například pomoct s náborem do firmy, ale můžete pomoct s rozvojem kompetencí. Pak nastala moderovaná diskuze nad tím, jaké cíle si stanovit. 

Při prvním plánování jsme se snažili definovat cíle vyloženě jako týmové. Představte si například cíl “zrychlit zpětnou vazbu” a jako měřitelné výsledky “na týdenní bázi měříme lead time” a “snížili jsme lead time o 5 %”. Když stanovíte všechny cíle jako týmové, tedy co by pomohlo týmu, tak se může stát, že máte cíle, o které se zajímá  50 % lidí v týmu. Co může být lepší, tak začít u druhého konce. Zeptat se každého, jak by chtěl přispět týmu a projektu. Tyto osobní cíle pak přeformulovat na týmové cíle, tak víte jistě, že si každý stanovil aspoň jeden cíl, kterým posune tým dopředu. Na konci je to hlavně o formulaci, ale cesta, jak se k tomu cíli dojde, je samozřejmě také důležitá. Pokud ty cíle vyloženě vychází od lidí, jen se usměrní do prostoru vymezeném stakeholdery, tím docílíte zvýšení angažovanosti. Ještě je třeba to držet v mezích týmovosti, nesklouznout k plánování cílů jednotlivců, což by mělo být v osobních rozvojových plánech. Dát si tedy tu práci s tou formulací, aby bylo jasné a patrné, že to pomáhá týmu a projektu.

Další úskalí leží v měřitelnosti výsledků, která je také o správné formulaci. Pokud si stanovíte například cíl “rozšířit kompetenci” a měřitelný výsledek “umíme pracovat s keycloakem”, jak budete měřit progres? Co znamená 20 %, 50 % a co 100 %? Zde je obecný problém, jak čistě a přesně můžete nějakou metodiku či techniku zavádět. Pokud jsou všichni na stejné vlně a mají podobnou znalostní bázi, tak může být zavádění jednoduché. Nebo pokud jde o management, který má strategické plánování v popis práce, tak je také jednoduché bavit se o přesnosti a možné tlačit na formulaci. Pokud ale přicházíte s něčím novým a k lidem, kteří prakticky nemusí spolupracovat, pokud si je nezískáte, je důležité myslet na to, že to nebude čisté ani přesné, dokonalé. Nakonec stejně dojdete k nějakému uzpůsobování na vlastní potřeby a možnosti, bavíme se tedy jen o tom, jak moc tlačit na začátku, než přejdete do nějakého postupného zlepšování. Platí zde známé pravidlo, že si lidé musí na některé věci přijít sami, jde tedy o mitigaci rizik selhání. Takže co se měřitelnosti týká, osvědčilo se mi netlačit na formulaci, protože na to lidé přijdou sami, až se je budete později ptát, jaký je progres. Takže v průběhu kvartálu se výše uvedený měřitelný výsledek může změnit na “alespoň 3 vývojáři ví, jak nastavit keycloak pro projekt”. Dobré je, se vždy zaměřit na to, že cíl definuje nějaký dopad, stav po nějakém období. Měřitelný výsledek pak popisuje jak vypadá úspěšné dosažení toho stavu.

### 3. Kontrola a vyhodnocení

Práce na týmových cílech je prakticky návyk. Je hrozně lehké nechat se pohltit projektovou operativou a zapomenout na nějaké rozvojové aktivity, které tu práci však mohou zjednodušit, zpříjemnit, zkvalitnit. Při práci s týmovými cíli je tedy důležité připomínání a sdílení. Sdílením myslím například to, že se jednotliví lidé v týmu nestydí pochlubit, když se jim něco povede. Stejně tak se nebojí poprosit o pomoc či radu, když si neví rady nebo si nejsou jistí, že je měřitelný výsledek cíle stále naplnitelný či relevantní. Připomínáním myslím to, že lidi neustále navádíte na to, že nějaké cíle jsou, že nám mají pomoct v posunu a že mají nějaké úkoly, které si sami vymysleli. Obě dvě aktivity, sdílení a připomínání, lze ve vývoji jednoduše zakomponovat do denních standupů, případně jinou formou, ale co nejpravidelněji.

Vedle připomínání a sdílení je důležité vyhodnocení progresu a kontrola relevantnosti. Jedná se o tzv. check-in meetingy, které jsme na začátku dělali každý týden, teď již každý druhý. Jednoduše projedete jednotlivé cíle a jejich měřitelné výsledky, které ohodnotíte (0-100 %, nebo 0.0-1.0). Také dále rozpadnete měřitelné výsledky na akční kroky, které vás dále posunou v plnění cíle. Není tedy důležité udělat kompletní rozpad na začátku, stačí jen pár akčních kroků, kterými začnete, a později je rozšíříte. Je to také prostor pro to, zamyslet se, jestli je cíl či měřitelný výsledek stále relevantní. Nemá cenu pracovat celý kvartál či rok na cíli, který už vám v ničem nepomůže. Stejně tak pokud někdo dokončí svůj cíl dříve a nenajde se v jiném cíli, tak si může stanovit nový cíl na zbytek časového rámce. 

### 4. Retrospektiva

Na konci časového rámce je dobré se s týmem posadit a říct si zpětnou vazbu. Zjistit, jak se jim s cíli pracovalo, jak jim to šlo, jestli jim něco nechybělo, nebo jestli se třeba nedozvěděli něco, co by se mohlo hodit ostatním. Pokud s týmovými cíli začínáte a jedná se o experiment, je to tedy příležitost, při které se týmu zeptáte, zda-li v tom vidí přínos a chtějí pokračovat.

## Naše výsledky po prvním kvartálu

Z čeho jsem měl obrovskou radost, tak si tým na zpětných vazbách se svým personal lídrem koncept chválili. Poskytl jim příležitosti, jak si vyzkoušet věci, které je zajímaly. Díky formě kontrol a vyhodnocování je to donutilo se těm věcem věnovat, což by třeba sami nezvládli a zájem by buď opadl nebo by se vytratila pozornost. Celková investice byla něco pod 4MD (dohromady), což bylo podle očekávání. Co bylo velice náročné, tak udělat z týmových cílů návyk, aby na nich lidé v týmu pravidelně a disciplinovaně pracovali. Stejně tak udržet motivaci a zájem, pokud se z týmových cílů začne stahovat ambasador a hlavní tahoun. Ideální je takovou osobu hned nahradit jinou, aby tam byl stále někdo, kdo realizaci posouvá vpřed a inspiruje ostatní. Celkově bych řekl, že je to docela boj, ale je potřeba ho vést, aby všichni v týmu věděli, že věci mohou posouvat dopředu a taky jak vypadá progres.



